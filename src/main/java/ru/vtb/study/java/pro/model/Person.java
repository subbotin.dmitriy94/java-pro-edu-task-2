package ru.vtb.study.java.pro.model;

/**
 * Person.
 *
 * @author Dmitriy Subbotin
 */
public class Person {

  private final String name;

  private final Integer age;

  private final Position position;

  public Person(String name, Integer age, Position position) {
    this.name = name;
    this.age = age;
    this.position = position;
  }

  public String getName() {
    return name;
  }

  public Integer getAge() {
    return age;
  }

  public Position getPosition() {
    return position;
  }

  @Override
  public String toString() {
    return "Person{" +
        "name='" + name + '\'' +
        ", age=" + age +
        ", position=" + position +
        '}';
  }
}

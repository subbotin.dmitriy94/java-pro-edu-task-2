package ru.vtb.study.java.pro;

import ru.vtb.study.java.pro.model.Person;
import ru.vtb.study.java.pro.model.Position;
import ru.vtb.study.java.pro.util.CollectionUtils;

import java.util.Arrays;
import java.util.List;

/**
 * Task-2.
 *
 * @author Dmitriy Subbotin
 */
public class Main {

  public static void main(String[] args) {

    List<Integer> integerList1 = List.of(99, 88, 1, 10, 88, 99, 99, 88, 5, 8, 1, 2, 2);

    System.out.println("Input collections:");
    System.out.println(integerList1);
    System.out.println("After remove duplicates:");
    System.out.println(CollectionUtils.deleteDuplicates(integerList1));

    System.out.println("Input collections:");
    System.out.println(integerList1);
    System.out.println("After remove duplicates (get Set):");
    System.out.println(CollectionUtils.deleteDuplicatesToSet(integerList1));


    List<Integer> integerList2 = List.of(5, 2, 10, 9, 4, 3, 10, 1, 13);

    System.out.println("Input collections:");
    System.out.println(integerList2);
    System.out.println("Third max:");
    System.out.println(CollectionUtils.getThirdMax(integerList2));

    System.out.println("Input collections:");
    System.out.println(integerList2);
    System.out.println("Third unique max:");
    System.out.println(CollectionUtils.getUniqueThirdMax(integerList2));


    List<Person> personList =
        List.of(
            new Person("Max1", 27, Position.ENGINEER),
            new Person("Max2", 30, Position.MANAGER),
            new Person("Max3", 23, Position.ENGINEER),
            new Person("Max4", 32, Position.DIRECTOR),
            new Person("Max5", 30, Position.ENGINEER),
            new Person("Max6", 27, Position.ENGINEER),
            new Person("Max7", 30, Position.MANAGER),
            new Person("Max8", 32, Position.MANAGER),
            new Person("Max9", 31, Position.DIRECTOR));

    System.out.println("Input collections:");
    System.out.println(personList);
    System.out.println("Output collections (Three oldest engineers):");
    System.out.println(CollectionUtils.getThreeOldestEngineers(personList));

    System.out.println("Input collections:");
    System.out.println(personList);
    System.out.println("Output avg age of engineers:");
    System.out.println(CollectionUtils.getAverageAgeOfEngineers(personList));


    List<String> words = List.of("Aa", "Bb", "Bbbb", "CCCCcccc", "ddddd", "eeee", "ffffffff");

    System.out.println("Input collections:");
    System.out.println(words);
    System.out.println("Longest word:");
    System.out.println(CollectionUtils.getLongestWord(words));


    String s = "dog cat mouse bear cat cat dog";

    System.out.println("Input string:");
    System.out.println(s);
    System.out.println("Convert to hashmap value -> count:");
    System.out.println(CollectionUtils.getWordCountMap(s));


    List<String> animals = List.of("dolphin", "duck", "dog", "camel", "mouse", "cow", "cat", "capybara", "monkey");

    System.out.println("Input collections:");
    System.out.println(animals);
    System.out.println("Sorted collection by length, then by alphabet:");
    System.out.println(CollectionUtils.sortByLengthThenByAlphabet(animals));


    String[] stringArray = new String[]{"dolphin duck dog camel tiger", "mouse cow cat capybara monkey"};

    System.out.println("Input collections:");
    System.out.println(Arrays.toString(stringArray));
    System.out.println("Get the longest word:");
    System.out.println(CollectionUtils.getLongestWord(stringArray));
  }
}
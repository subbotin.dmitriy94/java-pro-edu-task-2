package ru.vtb.study.java.pro.model;

/**
 * Position.
 *
 * @author Dmitriy Subbotin
 */
public enum Position {

  MANAGER,
  DIRECTOR,
  ENGINEER
}
